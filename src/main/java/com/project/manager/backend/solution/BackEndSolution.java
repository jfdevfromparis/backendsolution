package com.project.manager.backend.solution;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackEndSolution {

	public static void main(String[] args) {
		SpringApplication.run(BackEndSolution.class, args);
		System.out.println("http://localhost:8083/BackEndSolution/");
	}

}
